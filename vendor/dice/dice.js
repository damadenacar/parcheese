var _CLASS_POSITION = 0;

$.fn.dice = function(options, argument) {
    const defaults = {
        rollclasses: [ "roll1", "roll2" ],
        size: "100px",
        halfsize: "50px",
        dotsize: "10px",
        maxreps: 100
    }

    function sample(a){
        return a[Math.floor(Math.random()*a.length)];
    }

    var settings = $.extend(defaults, options);
    var originalsettings = Object.assign({}, settings);

    let vs = [];
    this.each(function() {
        let $this = $(this);
        if ($this.is('div')) {
            settings = Object.assign({}, originalsettings);
            if ($this.attr('dice-size') !== undefined)
                settings.size = $this.attr('dice-size');
            if ($this.attr('dice-halfsize') !== undefined)
                settings.halfsize = $this.attr('dice-halfsize');
            if ($this.attr('dice-dotsize') !== undefined)
                settings.dotsize = $this.attr('dice-dotsize');

            const transforms = [
                `rotate3d(0, 0, 0, 90deg) translateZ(${settings.halfsize})`,
                `rotate3d(-1, 0, 0, 90deg) translateZ(${settings.halfsize})`,
                `rotate3d(0, 1, 0, 90deg) translateZ(${settings.halfsize})`,
                `rotate3d(0, -1, 0, 90deg) translateZ(${settings.halfsize})`,
                `rotate3d(1, 0, 0, 90deg) translateZ(${settings.halfsize})`,
                `rotate3d(1, 0, 0, 180deg) translateZ(${settings.halfsize})`
            ];

            let data = this._diceinfo;

            if (data === undefined) {
                data = {};
                let faces = [];
                [1, 2, 3, 4, 5, 6].forEach(function(dots) {
                    faces[dots] = $(`<li class="face" data-side="${dots}"></li>`);
                    faces[dots].css('transform', transforms[dots - 1]);
                    for (let i = 0; i < dots; i++)
                        faces[dots].append($(`<span></span>`).addClass('dot').css('width', settings.dotsize).css('height', settings.dotsize));
                })

                let list = $(`<ol class="face-list">`).append(...faces).css('width', settings.size).css('height', settings.size);
                $this.addClass('dice').attr('data-value', 1).addClass(settings.rollclasses[_CLASS_POSITION]).empty();
                _CLASS_POSITION = (_CLASS_POSITION + 1) % settings.rollclasses.length;
                $this.append(list);   
            }

            this._diceinfo = data;
            
            function getRandomNumber(min, max) {
                min = Math.ceil(min);
                max = Math.floor(max);
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }

            switch (options) {
                case 'roll':
                    let classes = $this.attr('class').split(/\s+/);
                    let existing = settings.rollclasses.filter(function(n) {
                        return classes.indexOf(n) !== -1;
                    });
                    let current = existing[0];
                    if (current === undefined)
                        current = sample(settings.rollclasses);
                    
                    let maxreps = settings.maxreps;
                    let possibles = settings.rollclasses;


                    if ((settings.rollgraph !== undefined) && (settings.rollgraph[current] !== undefined))
                        possibles = settings.rollgraph[current];

                    while ((maxreps>0)&&(existing.indexOf(current) !== -1)) {
                        current = sample(possibles);
                        maxreps--;
                    }
                    
                    $this.removeClass(settings.rollclasses.join(" ")).addClass(current);

                    let number;
                    if ((typeof(argument) === "number") && (!isNaN(argument))) {
                        number = argument;
                    } else {
                        number = getRandomNumber(1, 6);
                    }

                    $this.attr('data-value', number);
                    break;
                case 'rollclass':
                    if (settings.rollclasses.indexOf(argument) !== -1)
                        $this.removeClass(settings.rollclasses.join(" ")).addClass(argument);
                    break;
                case 'value':
                    let v = parseInt($this.attr('data-value'));
                    if ((v >= 1) && (v <= 6)) {
                        vs.push(v);
                    } else
                        vs.push(null);
                    break;
            }

        } else {
            console.warn('the selected item does not contain a div')
        }
    })

    // To chain operations
    switch(options) {
        case 'value':
            if (vs.length === 1) return vs[0];
            return vs;
            break;
    }
    return this;
}        

$(function() {
    $('div[role="dice"]').dice();
})