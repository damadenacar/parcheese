/**
 * Function that obtains a random integer in a range (including both limits)
 * @param min lower limit
 * @param max upper limit
 * @return a random integer
 */

function getRandomNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Function that generates a random identifier built by numbers and letters, of a given length
 * @param length length of the identifier in characters
 * @return the random identifier
 */
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
