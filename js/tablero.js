const GameBoardDef = {
    name: "Parcheese",
    size: { width: 500, height: 497},
    cellsec: [
        5, 12, 17, 22, 29, 34, 39, 46, 51, 56, 63, 68
    ],
    celldefs: {
        0: [
            {   c: [ "fa0", "fa1", "fa2", "fa3"], top: 285  },
            {   c: [ "fr0", "fr1", "fr2", "fr3"], top: 205  },
            {   c: [ "fr0", "fa0" ], left: 220 },
            {   c: [ "fr1", "fa1" ], left: 239 },
            {   c: [ "fr2", "fa2" ], left: 258 },
            {   c: [ "fr3", "fa3" ], left: 277 },

            {   c: [ "fv0", "fv1", "fv2", "fv3"], left: 208  },
            {   c: [ "fz0", "fz1", "fz2", "fz3"], left: 290  },
            {   c: [ "fv0", "fz0" ], top: 217 },
            {   c: [ "fv1", "fz1" ], top: 236 },
            {   c: [ "fv2", "fz2" ], top: 255 },
            {   c: [ "fv3", "fz3" ], top: 274 },


            {
                c: [ "dr" ],
                left: 93, top: 97
            },
            {
                c: [ "dz" ],
                left: 403, top: 97
            },
            {
                c: [ "da" ],
                left: 403, top: 399
            },
            {
                c: [ "dv" ],
                left: 93, top: 399
            },
            // Las casas
            {
                c: [ "hr0", "hr1", "hz0", "hz1" ],
                top: 69
            },
            {
                c: [ "hr2", "hr3", "hz2", "hz3" ],
                top: 118
            },
            {
                c: [ "hv0", "hv1", "ha0", "ha1" ],
                top: 373
            },
            {
                c: [ "hv2", "hv3", "ha2", "ha3" ],
                top: 422
            },

            {
                c: [ "hr0", "hr2", "hv0", "hv2" ],
                left: 64
            },
            {
                c: [ "hr1", "hr3", "hv1", "hv3" ],
                left: 124
            },
            {
                c: [ "hz0", "hz2", "ha0", "ha2" ],
                left: 373
            },
            {
                c: [ "hz1", "hz3", "ha1", "ha3" ],
                left: 434
            },


            {   c: [ 35, 36, 37, 38, 39, 40, 41, 61, 62, 63, 64, 65, 66, 67 ],
                left: 198  },
            {   c: [ 1, 2, 3, 4, 5, 6, 7, 27, 28, 29, 30, 31, 32, 33 ],
                left: 300  },
            {   c: [ 34, "r1", "r2", "r3", "r4", "r5", "r6", "r7", "a1", "a2", "a3", "a4", "a5", "a6", "a7", 68 ],
                left: 249  },
            {   c: [ 33, 34, 35 ],
                top: 28   },
            {   c: [ 32, "r7", 36 ],
                top: 50   },
            {   c: [ 31, "r6", 37 ],
                top: 72   },
            {   c: [ 30, "r5", 38 ],
                top: 94   },
            {   c: [ 29, "r4", 39 ],
                top: 115   },
            {   c: [ 28, "r3", 40 ],
                top: 137   },
            {   c: [ 27, "r2", 41 ],
                top: 159   },
            {   c: [ 26, "r1", 42 ],
                top: 180   },
            {   c: [ 42, 60 ],
                left: 202   },
            {   c: [ 60, "a1", 8 ],
                top: 311   },
            {   c: [ 61, "a2", 7 ],
                top: 333   },
            {   c: [ 62, "a3", 6 ],
                top: 354   },
            {   c: [ 63, "a4", 5 ],
                top: 376   },
            {   c: [ 64, "a5", 4 ],
                top: 398   },
            {   c: [ 65, "a6", 3 ],
                top: 420   },
            {   c: [ 66, "a7", 2 ],
                top: 441   },
            {   c: [ 67, 68, 1 ],
                top: 463   },
            {   c: [ 26, 8 ],
                left: 295   },

            // Las lineas horizontales (superior, central, inferior)
            {   c: [ 44, 45, 46, 47, 48, 49, 50, 18, 19, 20, 21, 22, 23, 24],
                top: 195  },

            {   c: [ 51, "v1", "v2", "v3", "v4", "v5", "v6", "v7", "z1", "z2", "z3", "z4", "z5", "z6", "z7", 17 ],
                top: 245  },

            {   c: [ 52, 53, 54, 55, 56, 57, 58, 10, 11, 12, 13, 14, 15, 16 ],
                top: 296  },

            {   c: [ 50, 51, 52 ],
                left: 27   },
            
            // Zona verde
            {   c: [ 49, "v7", 53 ],
                left: 50   },
            {   c: [ 48, "v6", 54 ],
                left: 72   },
            {   c: [ 47, "v5", 55 ],
                left: 94   },
            {   c: [ 46, "v4", 56 ],
                left: 116   },
            {   c: [ 45, "v3", 57 ],
                left: 139   },
            {   c: [ 44, "v2", 58 ],
                left: 161   },
            {   c: [ 43, "v1", 59 ],
                left: 183 },

            // Zona azul
            {   c: [ 9, "z1", 25 ],
                left: 316  },  
            {   c: [ 10, "z2", 24 ],
                left: 338   },
            {   c: [ 11, "z3", 23 ],
                left: 360   },
            {   c: [ 12, "z4", 22 ],
                left: 382   },
            {   c: [ 13, "z5", 21 ],
                left: 404   },
            {   c: [ 14, "z6", 20 ],
                left: 426   },
            {   c: [ 15, "z7", 19 ],
                left: 448   },
            {   c: [ 16, 17, 18 ],
                left: 470   },

            // Esquinas horizontales
            {   c: [ 25, 43 ],
                top: 200  },
                
            {   c: [ 9, 59 ],
                top: 291  },
        ],
        1: [
            {   c: [ 59 ], left: 180, top: 302 },
            {   c: [ 60 ], left: 191, top:314 },
            {   c: [ 8 ], left: 307, top: 314 },
            {   c: [ 9 ], left: 318, top: 302 },
            {   c: [ 25 ], left: 318, top: 189 },
            {   c: [ 42 ], left: 191, top: 178 },
            {   c: [ 26 ], left: 307, top: 178 },
            {   c: [ 43 ], left: 180, top: 189 },

            {   c: [ 35, 36, 37, 38, 39, 40, 41, 61, 62, 63, 64, 65, 66, 67 ],
                dleft: -12  }, // 198
            {   c: [ 1, 2, 3, 4, 5, 6, 7, 27, 28, 29, 30, 31, 32, 33 ],
                dleft: 12  }, // 300
            {   c: [ 34, "r1", "r2", "r3", "r4", "r5", "r6", "r7" ],
                dleft: -12  }, // 249
            {   c: [  "a1", "a2", "a3", "a4", "a5", "a6", "a7", 68 ],
                dleft: 12  }, // 249
            {   c: [ 44, 45, 46, 47, 48, 49, 50, 18, 19, 20, 21, 22, 23, 24],
                dtop: -12  },    // 195    
            {   c: [ 51, "v1", "v2", "v3", "v4", "v5", "v6", "v7" ],
                dtop: -12  },    // 245
            {   c: [ "z1", "z2", "z3", "z4", "z5", "z6", "z7", 17 ],
                dtop: 12  },    // 245
            {   c: [ 52, 53, 54, 55, 56, 57, 58, 10, 11, 12, 13, 14, 15, 16 ],
                dtop: 12  },    // 296
            ],    
        2: [
            {   c: [ 59 ], left: 183, top: 284 },
            {   c: [ 60 ], left: 210, top: 311 },
            {   c: [ 8 ], left: 289, top: 311 },
            {   c: [ 9 ], left: 314, top: 284 },
            {   c: [ 25 ], left: 314, top: 207 },
            {   c: [ 42 ], left: 210, top: 180 },
            {   c: [ 26 ], left: 289, top: 180 },
            {   c: [ 43 ], left: 183, top: 207 },

            {   c: [ 35, 36, 37, 38, 39, 40, 41, 61, 62, 63, 64, 65, 66, 67 ],
                dleft: 12  }, // 198
            {   c: [ 1, 2, 3, 4, 5, 6, 7, 27, 28, 29, 30, 31, 32, 33 ],
                dleft: -12  }, // 300
            {   c: [ 34, "r1", "r2", "r3", "r4", "r5", "r6", "r7" ],
                dleft: 12  }, // 249
            {   c: [  "a1", "a2", "a3", "a4", "a5", "a6", "a7", 68 ],
                dleft: -12  }, // 249
            {   c: [ 44, 45, 46, 47, 48, 49, 50, 18, 19, 20, 21, 22, 23, 24],
                dtop: 12  },    // 195    
            {   c: [ 51, "v1", "v2", "v3", "v4", "v5", "v6", "v7" ],
                dtop: 12  },    // 245
            {   c: [ "z1", "z2", "z3", "z4", "z5", "z6", "z7", 17 ],
                dtop: -12  },    // 245
            {   c: [ 52, 53, 54, 55, 56, 57, 58, 10, 11, 12, 13, 14, 15, 16 ],
                dtop: -12  },    // 296
            ]
        }
}



// parse(GameBoardDef);

class Tablero {
    _parse(t_def) {
        let tablero = {
            width: 0,
            height: 0,
            posiciones: []
        };
        let c_posiciones = [];
    
        let celldefs = t_def.celldefs;
    
        for (let t_cell in celldefs) {
            let posiciones = [];
            celldefs[t_cell].forEach(function(c_def) {
                c_def.c.forEach(function(cell_i) {
                    if (! (cell_i in posiciones)) {
                        posiciones[cell_i] = { left: undefined, top: undefined};
                    }
                    if (c_def.left !== undefined) posiciones[cell_i].left = c_def.left;
                    if (c_def.top !== undefined) posiciones[cell_i].top = c_def.top;
                    if (t_cell != 0) {
                        if (c_def.dleft !== undefined) {
                            posiciones[cell_i].left = c_posiciones[0][cell_i].left + c_def.dleft;
                            if (posiciones[cell_i].top === undefined) 
                                posiciones[cell_i].top = c_posiciones[0][cell_i].top;
                        }
                        if (c_def.dtop !== undefined) {
                            posiciones[cell_i].top = c_posiciones[0][cell_i].top + c_def.dtop;
                            if (posiciones[cell_i].left === undefined) 
                                posiciones[cell_i].left = c_posiciones[0][cell_i].left;
                        }
                    }
                })
            })
            c_posiciones[t_cell] = posiciones;

            if (! (0 in c_posiciones))
                throw `position 0 must be first defined`;
        }

        for (let i in c_posiciones) {
            if (i === 0) continue;

            c_posiciones[i].forEach(function(v, c_id) {
                if (c_posiciones[0][c_id] === undefined) {
                    throw `cell ${c_id} must be also defined for position 0`
                }
                if (v.left === undefined) {
                    if (c_posiciones[0][c_id].left === undefined)
                        throw `cell ${c_id} at side ${i} is not fully defined`
                    v.left = c_posiciones[0][c_id].left;
                }
                if (v.top === undefined) {
                    if (c_posiciones[0][c_id].top === undefined)
                        throw `cell ${c_id} at side ${i} is not fully defined`
                    v.top = c_posiciones[0][c_id].top;
                }
            })
        }

        tablero.width = t_def.size.width;
        tablero.height = t_def.size.height;
        tablero.posiciones = c_posiciones;
        tablero.seguros = t_def.cellsec;

        return tablero;
    }

    seguro(cellid) {
        return this.tablero.seguros.indexOf(cellid) !== -1;
    }

    constructor($parent, t_def) {
        let tablero = this._parse(t_def);

        if ((typeof(tablero.width) !== "number") || (tablero.width <= 0))
            throw `invalid width ${tablero.width}`;
        if ((typeof(tablero.height) !== "number") || (tablero.height <= 0))
            throw `invalid height ${tablero.height}`;
        for (let t_pos in tablero.posiciones) {
            tablero.posiciones[t_pos].forEach(function(v) {
                if ((v.left === undefined) || (v.top === undefined))
                    throw `invalid element ${v}`;
            });
        }

        let w = $parent.width();
        let h = $parent.height();

        let rw = w / tablero.width;
        let rh = h / tablero.height;
        let rmin = rw>rh?rh:rw;

        w = tablero.width * rmin;
        h = tablero.height * rmin;

        this.rx = (1.0 * w) / tablero.width;
        this.ry = (1.0 * h) / tablero.height;
        this.tablero = tablero;
        this.$parent = $parent;
    }

    offset(cell, side = 0, $element) {
        let ox = -$element.outerWidth() / 2;
        let oy = -$element.outerHeight() / 2;
        let p_offset = this.$parent.offset();
        let offset;
        switch (side) {
            case 1:
                offset = this._offset_other(cell, 1);
                break;
            case 2:
                offset = this._offset_other(cell, 2);
                break;
            default:
                offset = this._offset_center(cell);
                break;
        }
        return {top:p_offset.top + offset.top + ox, left: p_offset.left + offset.left + oy};
    }

    _offset_center(cell) {
        if (cell in this.tablero.posiciones[0]) {
            let p = this.tablero.posiciones[0][cell];
            return {left: (p.left * this.rx), top: (p.top * this.ry) };
        } else {
            throw `invalid cell ${cell}`;
        }
        
    }
    _offset_other(cell, preference) {
        let p = null;
        if ((this.tablero.posiciones[preference] !== undefined) && (this.tablero.posiciones[preference][cell] !== undefined)) {
            p = this.tablero.posiciones[preference][cell];
        } else if (this.tablero.posiciones[0][cell] !== undefined) {
            p = this.tablero.posiciones[0][cell];
        }
        if (p !== null) {
            return {left: (p.left * this.rx), top: (p.top * this.ry) };
        } else {
            throw `invalid cell ${cell}`;
        }   
    }
}

