class Player {
    constructor(type, dado, name) {
        this._dado = dado;

        if (name === undefined) {
            name = "player_" + makeid(2);
        }
        
        if ([ F_ROJO, F_AMARILLO, F_VERDE, F_AZUL ].indexOf(type) === -1) 
            throw new Error(`type ${type} for player is invalid`);

        this._type = type;
        this._name = name;
        this._endrollcallback = null;
        this._interactive = false;
        this._misfichas = [];
    }

    get dado() { return this._dado; }
    set dado(d) { this._dado = d; }
    get interactivo() { return this._interactive; }
    get tipo() {return this._type;}
    get name() {return this._name;}
    get tipoc() {
        switch (this._type) {
            case F_ROJO: return "r";
            case F_VERDE: return "v";
            case F_AMARILLO: return "a";
            case F_AZUL: return "z";
        }
        return null;
    }

    asignarfichas(fichas) {
        this._misfichas = [ ...fichas ];
    }

    cleanup() {
        this._dado.cleanup();
    }

    /**
     * Funcion que se debe llamar para indicar que el jugador debe efectuar su tirada
     * @param endrollcallback funcion que se debe llamar al acabar de tirar los dados y que tiene 
     */
    forzartirada(valortirada) {
        let self = this;
        if (typeof(this._endrollcallback) === "function")
            this._dado.roll(valortirada, function() {
                self._endrollcallback(valortirada, self);
            });
        else
            this._dado.roll(valortirada);
    }

    tirar(valortirada, endrollcallback = null) {
        this._endrollcallback = endrollcallback;

        let self = this;
        self.forzartirada(valortirada);

        this._dado.$.addClass('turno');
    }

    turno() {
        this._dado.$.addClass('turno');
        this._misfichas.forEach((x) => (x.$.addClass('turno')));
    }

    forzarseleccionficha(fichas, tablero, posiciones) {
        if (fichas.length === 0)
            fichas = [ ];

        // Ahora tenemos que aplicar una heuristica entre las posibles fichas
        let m_puntos, m_ficha = fichas[0];

        if (fichas.length > 1)
            fichas.forEach(function(ficha) {
                let reasoning = "";
                let c_puntos = 0;
                let casilladestino = ficha.destpos(posiciones);

                if (casilladestino === 0) {
                    // nos apuntamos 10 puntos si vamos a casa
                    c_puntos += 20;
                    reasoning = reasoning + "F";
                } else 
                if (casilladestino < 0) {
                    // Si vamos al camino a casa, nos apuntamos 10
                    c_puntos += 10;
                    reasoning = reasoning + "H";
                } else {
                    // Miramos a ver cuanto nos queda y si es poco, nos apuntamos la cuarta parte (hasta 7 puntos)
                    let casillasacasa = ficha.cells2homeway();
                    if (casillasacasa < 28)
                        c_puntos += (casillasacasa / 4);
                    reasoning = reasoning + `C(${casillasacasa})`;
                }

                // Si vamos a un seguro, son mas puntos
                if (tablero.tablero_def.seguro(casilladestino)) {
                    reasoning = reasoning + "S";
                    c_puntos += 10;
                } else {
                    // TODO: - penalizar si nos ponemos delante de alguien y no 

                    // Distancia de perseguidores
                    if (casilladestino > 0) {
                        let fichasfueradecasa = tablero.fichasfueradecasa();
                        fichasfueradecasa.forEach(function(f) {
                            if ((f.tipo !== ficha.tipo) && (f.currentcell > 0)) {
                                let f_cell = f.currentcell;
                                if (f_cell > casilladestino) {
                                    f_cell -= 68;
                                }
                                let distancia = casilladestino - f_cell;
                                if ((distancia > 0) && (distancia < 7)) {
                                    reasoning = reasoning + `T(${distancia})`
                                    c_puntos -= 15;
                                }
                            }
                        })
                    }                
                }

                // Si podemos matar, intentaremos matar
                if (tablero.mataficha(ficha, casilladestino) !== false) {
                    reasoning = reasoning + "M";
                    c_puntos += 40;
                }

                // Mantenemos la posicion en casa
                if (ficha.currentcell === ficha.principio) {
                    reasoning = reasoning + "K";
                    c_puntos -= 5;
                }

                // Mantenemos los seguros
                if (tablero.tablero_def.seguro(ficha.currentcell)) {
                    reasoning = reasoning + "U";
                    c_puntos -= 10;
                } else {
                    // TODO: - favorecer el escaparnos de alguien que nos tiene a tiro

                    // Distancia de perseguidores
                    if (ficha.currentcell > 0) {
                        let fichasfueradecasa = tablero.fichasfueradecasa();
                        let ficha_cell = ficha.currentcell;
                        fichasfueradecasa.forEach(function(f) {
                            if ((f.tipo !== ficha.tipo) && (f.currentcell > 0)) {
                                let f_cell = f.currentcell;
                                if (f_cell > ficha_cell) {
                                    f_cell -= 68;
                                }
                                let distancia = ficha_cell - f_cell;
                                if (distancia < 7) {
                                    reasoning = reasoning + `D(${distancia})`
                                    c_puntos += 15;
                                }
                            }
                        })
                    }
                }

                // Mantenemos los puentes fuera del camino a casa
                let fichaenpuente = tablero.puentes(ficha.tipo).reduce((res, fichas) => ([...res, ...fichas]), []).filter((x) => (x.id === ficha.id)).length === 1;
                if ((ficha.currentcell>0) && (fichaenpuente)) {
                    // Es mejor un puente en un seguro que en una casilla normal
                    if (tablero.tablero_def.seguro(ficha.currentcell)) {
                        c_puntos -= 10;
                    } else
                        c_puntos -= 5;

                    reasoning = reasoning + "P";
                }

                if ((m_puntos === undefined)||(c_puntos > m_puntos)) {
                    m_ficha = ficha;
                    m_puntos = c_puntos;
                }
            })

        if (typeof(this._fichaseleccionadacallback) === "function")
            this._fichaseleccionadacallback(m_ficha);
    }
    
    /**
     * 
     * @param fichas conjunto de fichas que puede mover el jugador
     * @param tablero el tablero de juego
     * @param posiciones el numero de posiciones que se avanzan
     * @param playedcallback funcion que se debe llamar al acabar de seleccionar la ficha
     */
    seleccionarfichaamover(fichas, tablero, posiciones, playedcallback = function(){} ) {
        let self = this;
        this._fichaseleccionadacallback = playedcallback;

        self.forzarseleccionficha(fichas, tablero, posiciones);
    }
}

class Player_Humano extends Player {
    /**
     * 
     * @param dado el dado que puede usar en el interfaz
     * @param type el tipo de jugador (en realidad, es el color)
     * @param name 
     */
    constructor(type, dado, name) {
        super(type, dado, name);
        this._valoresdado = null;
        this._interactive = true;
    }

    tirar(valortirada, endrollcallback = null) {
        let self = this;
        this._dado.setcallbacks(
            function(values, dado) {
                return valortirada;
            }, function() {
                self._dado.disable();
                if (typeof(endrollcallback) === 'function')
                    endrollcallback();
            });

        // Activamos el dado
        this._dado.enable().$.addClass('turno');
    }

    forzartirada(valortirada) {
        this._dado.disable();
        return super.forzartirada(valortirada);
    }

    forzarseleccionficha(fichas, tablero, posiciones) {
        fichas.forEach(function(f) {
            f.disable();
        })
        super.forzarseleccionficha(fichas, tablero, posiciones);
    }

    seleccionarfichaamover(fichas, tablero, posiciones, playedcallback = function(){} ) {
        this._fichaseleccionadacallback = playedcallback;

        if (fichas.length === 1) {
            playedcallback(fichas[0]);
            return;
        }

        // Si todas las fichas que se pueden jugar estan en la misma casilla, movemos una
        let casilla = fichas[0].cell2pos(fichas[0].currentcell);
        let fichasenlamismacasilla = fichas.reduce((a,x) => (a + (x.cell2pos(x.currentcell) === casilla?1:0)), 0);
        if (fichasenlamismacasilla === fichas.length) {
            playedcallback(fichas[0]);
            return;
        }

        this._misfichas.forEach((x) => (x.disable()));

        fichas.forEach(function(f) {
            f.off('click')
            .on('click', function() {
                if (typeof(playedcallback) === "function") {
                    playedcallback(f);
                }
            })
            .enable();
        });
    }

    fichaseleccionada(ficha, fichas) {
        fichas.forEach((x) => (x.disable()));
        if (typeof(this._playedcallback) === "function") {
            this._playedcallback(ficha);
        }
    }
}
