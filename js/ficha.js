const F_ROJO = 0;
const F_VERDE = 1;
const F_AMARILLO = 2;
const F_AZUL = 3

class Ficha {
    casa = -1;
    principio = 1;
    zonechar = "X";
    claseficha = null;
    tipo = null;

    _id = null;
    _pos = null;

    static create(t) {
        switch (t) {
            case F_ROJO: return new FichaRoja();
            case F_VERDE: return new FichaVerde();
            case F_AMARILLO: return new FichaAmarilla();
            case F_AZUL: return new FichaAzul();
            default: return null;
        }
    }

    on(method, callback = function() {}) {
        this.$.on(method, callback);
        return this;
    }
    off(method) {
        this.$.off(method);
        return this;
    }

    /**
     * Sets the current cell (should check if the cell is valid or not)
     */
    set currentcell(v) {
        switch (typeof(v)) {
            case 'number':
                if ((v < 0) || (v > 68)) throw `invalid cell ${v}`;
                break;
            case 'string':
                if (v[0] === 'f') {
                    if ((v.length !== 3) || (v[1] !== this.zonechar) || (v[2] < '0') || (v[2] > '3'))
                        throw `invalid cell ${v}`;
                    v = 0;
                } else
                if (v[0] === 'h') {
                    if ((v.length !== 3) || (v[1] !== this.zonechar) || (v[2] < '0') || (v[2] > '3'))
                        throw `invalid cell ${v}`;
                } else {
                    if ((v.length !== 2) || (v[0] !== this.zonechar) || (v[1] < '0') || (v[1] > '8'))
                        throw `invalid cell ${v}`;
                }
                break;
            default:
                throw `invalid cell ${v}`;
        }
        this._pos = v;
    }

    get currentcell() {
        return this._pos;
    }

    get currentpos() { return this.cell2pos(this._pos); }

    get $() {
        if (this._$ === undefined) {
            this._$ = this._create_html_object();
            $('#tablero').append(this._$);
        }
        return this._$;
    }

    cleanup() {
        if (this._$ !== undefined)
            this._$.remove();
    }

    enable(enable = true) {
        if (enable === true)
            this.$.removeClass("disabled");
        else
            this.$.addClass("disabled");
        return this;
    }

    disable() {
        return this.enable(false);
    }

    /**
     * Creates the HTML object that will represent the chip
     * @return the html object that represents the chip
     */
    _create_html_object() {
        this._id = makeid(8);
        let obj = $(`<div id="${this._id}" class="ficha"></div>`);
        if (this.claseficha !== null)
            obj = obj.addClass(this.claseficha);
        return obj;
    }

    get id() {
        return this._id;
    }

    /**
     * Places the chip in the starting cell
    start() {
        this.pos = this.principio;
        return this.pos;
    }
     */

    /**
     * Gets the next position for the chip
     * @param pos the position considered
     * @return the next position for the chip (or null, in case the position is null)
     */
    next(pos) {
        // If the chip has not started its journey, return null
        if (pos === null)
            return this.principio;

        // The chip has ended its journey
        if (pos === 0)  
            return 0;

        // If the position starts the way home, let's use the negative values
        if (pos == this.casa) {
            pos = -8;
        }

        // Advance one position
        pos = pos + 1;

        // The game board has 68 places and starts over
        if (pos > 68) pos = 1;

        // Return the position
        return pos;
    }

    cell2pos(v) {
        switch (typeof(v)) {
            case 'number':
                if ((v < 1) || (v > 68)) throw `invalid cell ${v}`;
                return v;
                break;
            case 'string':
                if (v[0] === 'h') {
                    if ((v.length !== 3) || (v[1] !== this.zonechar) || (v[2] < '0') || (v[2] > '3'))
                        throw `invalid cell ${v}`;

                    // The chip is at home
                    return null;
                } else {
                    if ((v.length !== 2) || (v[0] !== this.zonechar) || (v[1] < '0') || (v[1] > '8'))
                        throw `invalid cell ${v}`;
                    
                    let hpos = parseInt(v[1]);
                    return -hpos;
                }
                break;
            default:
                throw `invalid cell ${v}`;
        }        
    }

    /**
     * Returns the list of cells that the chip needs to go through to move "count" moments
     * @param count the amount of steps that the chip needs to go through
     * @param pos the position where the cell is (not set to get the current position)
     * @return an array that contains the ids of the cells where the chip will be, or null if
     *         position is null
     */
    breadcrumb(pos, count) {
        /** */
        if (pos === null)
            return null;

        let breadcrumb = [];
        while (count > 0) {
            let prevpos = pos;
            pos = this.next(pos);
            if (pos === prevpos)
                return false;

            breadcrumb.push(pos);
            count--;
        }
        return breadcrumb;
    }

    /**
     * Funcion que devuelve la posicion de destino, sin tener en cuenta posibles puentes ni cosas asi
     *   util especialmente cuando ya se sabe que se puede mover, para averiguar la casilla de destino
     *   sin tener que llamar a breadcrumb
     * @param count 
     */
    destpos(count) {
        let pos = this.cell2pos(this._pos);

        while (count > 0) {
            pos = this.next(pos);
            count--;
        }
        return pos;
    }

    cells2homeway() {
        let pos = this.cell2pos(this._pos);
        let count = 0;
        while (pos > 0) {
            pos = this.next(pos);
            count ++;
        }
        return count;
    }

    /**
     * Returns the name of the cell in which the chip is (or null if the position of the cell is null)
     */
    pos2cell(pos) {
        if (pos === 0) return 0;
        if (pos === null) return null;
        if (pos > 0) return pos;
        return this.zonechar + (-pos);
    }

    /**
     * Sets the placement for the html object
     */
    placehtml(placement) {
        this.$.offset(placement);
    }

    movehtml(placement, f = function() {}) {
        this.$.animate(placement, null, "swing", f);
    }

    movehtmlto(tablero, casilla_id, i, f = function() {}) {
        if (this.currentcell !== casilla_id) {
            let self = this;
            let nextpos = this.next(this.cell2pos(this.currentcell));
            this.currentcell = nextpos;
            let p = tablero.offset(nextpos, 0, this.$);

            this.$.animate(p, "fast", "swing", function() {
                self.movehtmlto(tablero, casilla_id, i, f);
            })
        } else {
            f();
        }
    }
}

class FichaRoja extends Ficha {
    casa = 34;
    principio = 39;
    zonechar = "r";
    claseficha = "rojo";
    tipo = F_ROJO;
}         

class FichaVerde extends Ficha {
    casa = 51;
    principio = 56;
    zonechar = "v";
    claseficha = "verde";
    tipo = F_VERDE;
}         

class FichaAmarilla extends Ficha {
    casa = 68;
    principio = 5;
    zonechar = "a";
    claseficha = "amarillo";
    tipo = F_AMARILLO;
}         

class FichaAzul extends Ficha {
    casa = 17;
    principio = 22;
    zonechar = "z";
    claseficha = "azul";
    tipo = F_AZUL;
}
