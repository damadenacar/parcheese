class TableroJuego {
    casillas = new CellArray();
    tiraotravez = false;
    fichamuerta = false;

    // TODO: resize de dados cuando haya resize de la ventana

    /**
     * Funcion que devuelve los puentes en los que hay involucradas fichas del tipo indicado o todos los puentes,
     *   en caso de que el tipo sea null
     * @param tipo tipo del que se quieren obtener los puentes
     */
    puentes(tipo = null, mixtos = true) {
        let casillas = this.casillas;
        if (tipo !== null) {
            casillas = casillas.filter((x) => (x.filter((f) => (f.tipo === tipo))).length > 0)
        }
        let puentes = casillas.filter((x) => (x.length === 2));

        if (mixtos !== true) {
            puentes = puentes.filter(function(casilla){
                return ((casilla[0].tipo === tipo) && (casilla[1].tipo === tipo));
            })
        }
        return puentes;
    }

    save() {
        return this.casillas.reduce((a,f,i) => ([...a,...f.map((x) => ({t:x.tipo,c:i}))]), [
            ...this._finalizadas.reduce((a,f) => [ ...a, ...f.map((x) => ({t:x.tipo,c:0}))
            ])
        ]);
    }

    load(s) {
        let casillas = new CellArray();
        s.forEach(function(def) { 
            let ficha = Ficha.create(def.t);
            if (casillas[def.c] === undefined) casillas[def.c] = [];
            console.log(def);
            casillas[def.c].push(ficha);
        })

        this.casillas.forEach(function(fichascasilla) {
            fichascasilla.forEach(function(x) { 
                x.$.remove();
            })
        });

        let finalizadas = [];
        [ F_ROJO, F_AMARILLO, F_VERDE, F_AZUL].forEach((x) => finalizadas[x] = []);
        if (casillas[0] !== undefined) {
            casillas[0].forEach(function(f) {
                finalizadas[f.tipo].push(f);
            })
            casillas.splice(0, 1);
        }

        this.casillas = casillas;
        this._finalizadas = finalizadas;

        let fichas = casillas.fichas();
        console.log(fichas);
        // reduce((total, a) => ([ ...total, ...a]), []);

        let casa = [];
        [ F_ROJO, F_AMARILLO, F_VERDE, F_AZUL].forEach(function(tipo) {
            let nfichas = fichas.filter((x) => (x.tipo === tipo)).length + finalizadas[tipo].length;
            casa[tipo] = [];
            for (let i = 0; i < 4-nfichas; i++) {
                casa[tipo].push(Ficha.create(tipo));
            }
        })

        /*
        this.home.forEach(function(tipo) {
            tipo.forEach(function(x) {
                if (x!==null)
                    x.$.remove();
            });
        })
        */
        this.fichasencasa().forEach(function(x) {
            x.$.remove();
        })
        this.home = casa;
        this.actualizar();
        this.fichas().forEach((x) => (x.disable()));
    }

    reinicia(jugadores = []) {
        this.fichas().forEach((x) => (x.cleanup()));

        this._finalizadas = [];
        this.home = [];

        let self = this;

        jugadores.forEach(function(p) {
            self._finalizadas[ p.tipo ] = [];
            self._ultimaficha = [];
            self.home[p.tipo] = [1,2,3,4].map((x) => Ficha.create(p.tipo))
        })

        this.casillas = new CellArray();
    }


    /**
     * Recibe la definicion del trablero, para obtener las coordenadas de cada casilla
     */
    constructor(tablero_def) {
        this.tablero_def = tablero_def;
        this.reinicia();
    }

    ultimaficha(tipo) {
        if (this._ultimaficha[tipo] === undefined)
            return null;
        return this._ultimaficha[tipo];
    }

    /**
     * Obtiene todas las posibles fichas, esten en la casa o en juego
     */
    fichas(tipo = null) {
        return [
            ...this.fichasencasa(tipo),
            ...this.casillas.fichas(tipo),
            ...this.fichasfinalizadas(tipo)
        ];
    }

    /**
     * Devuelve las fichas que hay en casa, de un tipo (o todas)
     * @param tipo el color que se quiere consultar, o "null" si se quieren todas
     * @return una lista con las fichas disponibles
     */
    fichasencasa(tipo = null) {
        if (this.home === undefined) {
            return [];
        }
        if (tipo === null) {
            return this.home.reduce((fichas, fichascasa) => ([ ...fichas, ...fichascasa]), []).filter((x) => (x!==null))
        } else {
            return this.home[tipo].filter((x) => (x!==null));
        }
    }

    /**
     * Devuelve las fichas que ya han acabadoo, de un tipo (o todas)
     * @param tipo el color que se quiere consultar, o "null" si se quieren todas
     * @return una lista con las fichas disponibles
     */
    fichasfinalizadas(tipo = null) {
        if (this._finalizadas === undefined)
            return [];
        if (tipo === null) {
            return this._finalizadas.reduce((fichas, fichasfinalizadas) => ([ ...fichas, ...fichasfinalizadas]), []).filter((x) => (x!==null))
        } else {
            return this._finalizadas[tipo].filter((x) => (x!==null));
        }
    }

    fichasfueradecasa(tipo = null) {
        return this.casillas.fichas(tipo);
    }

    /**
     * Estas son las fichas que aun no han finalizado (las que hay en casa + las que hay fuera de casa)
     */
    fichasenjuego(tipo = null) {
        return [
            ...this.fichasencasa(tipo),
            ...this.casillas.fichas(tipo)
        ];

    }

    /**
     * Funcion que se invoca de forma sincrona para actualizar las fichas en el tablero: para dibujarlas
     */
    actualizar() {
        let tablero_def = this.tablero_def;
        this.home.forEach(function(fichas, color) {
            let pos = 0;
            fichas.forEach(function(ficha) {
                if (ficha !== null) {
                    let homeplace = "h" + ficha.zonechar + pos;
                    let homepos = tablero_def.offset(homeplace, 0, ficha.$);
                    ficha.placehtml(homepos);
                    ficha.movehtml(homepos);
                    ficha.currentcell = homeplace;
                }
                pos = pos + 1;
            })
        })

        this._finalizadas.forEach(function(fichas, color) {
            let pos = 0;
            fichas.forEach(function(ficha) {
                if (ficha !== null) {
                    let endplace = "f" + ficha.zonechar + pos;
                    let endpos = tablero_def.offset(endplace, 0, ficha.$);
                    ficha.placehtml(endpos);
                    ficha.movehtml(endpos);
                    ficha.currentcell = endplace;
                }
                pos = pos + 1;
            })
        })

        this.casillas.forEach(function(fichas, casilla_id) {
            let nfichas = fichas.length;
            fichas.forEach(function(ficha, i) {
                if (nfichas > 1)
                    i++;

                let p = tablero_def.offset(casilla_id, i, ficha.$);
                ficha.placehtml(p);
                ficha.movehtml(p);
                ficha.currentcell = casilla_id;
            });
        })
    }

    /**
     * Funcion que se encarga de mover las fichas en el interfaz; es asíncrona y no sabemos cuanto tardará, así que hay 
     * un callback para cuando se terminen de mover las fichas.
     * @param endmovecallback callback que se llama cuando se terminan de mover las fichas
     */
    moverfichas(endmovecallback = function(){} ) {
        let tablero_def = this.tablero_def;
        let fichasparamover = 0;

        // TODO: hacer una funcion para que mueva todas las fichas pendientes y se llame una unica vez a "endmovecallback", porque ahora
        //   si se mueven varias a la vez (por ejemplo, cuando se llega a un puente, que se mueven 2 fichas, o cuando se mata una, se llamaria
        //   dos veces a endmovecallback)

        this.casillas.forEach(function(fichas, casilla_id) {
            fichasparamover += fichas.filter((ficha) => (ficha.currentcell !== casilla_id)).length;
        });

        let casillasadibujar = unique(this.casillas.reduce(function(total, fichascasilla, casilla_id) {
            if (fichascasilla.filter((ficha) => (ficha.currentcell !== casilla_id)).length > 0)
                return [ ...total, ...fichascasilla.map((x) => (x.currentcell)), casilla_id ]
            else
                return total;
        }, []));

        function callendcallback() {
            fichasparamover--;
            if ((fichasparamover === 0) && 
                (typeof(endmovecallback) === "function"))
                    endmovecallback();
        }


        // Primero dibujamos las de las casas, por si hay una que se ha matado, se tiene que llevar a la casa
        this.home.forEach(function(fichas, color) {
            let pos = 0;
            fichas.forEach(function(ficha) {
                if (ficha !== null) {
                    let homeplace = "h" + ficha.zonechar + pos;
                    let homepos = tablero_def.offset(homeplace, 0, ficha.$);
                    ficha.movehtml(homepos);
                    ficha.currentcell = homeplace;
                }
                pos = pos + 1;
            })
        })

        // Ahora dibujamos las que hayan llegado al final
        this._finalizadas.forEach(function(fichas, color) {
            let pos = 0;
            fichas.forEach(function(ficha) {
                if (ficha !== null) {
                    fichasparamover++;
                    let endplace = "f" + ficha.zonechar + pos;
                    let endpos = tablero_def.offset(endplace, 0, ficha.$);
                    ficha.movehtml(endpos, callendcallback);
                    ficha.currentcell = endplace;
                }
                pos = pos + 1;
            })
        })        

        if (fichasparamover === 0) {
            if (typeof(endmovecallback) === "function")
                endmovecallback();
        }
        else {
            let self = this;
            casillasadibujar.forEach(function(casilla_id) {
                let fichas = self.fichascasilla(casilla_id);
                let nfichas = fichas.length;
                fichas.forEach(function(ficha,i ) {
                    if (nfichas > 1)
                        i++;

                    if (false)
                        ficha.movehtmlto(tablero_def, casilla_id, i, callendcallback);
                    else {
                        let p = tablero_def.offset(casilla_id, i, ficha.$);
                        ficha.movehtml(p, callendcallback);
                        ficha.currentcell = casilla_id;
                    }
                }) 
            });

            /*
            this.casillas.forEach(function(fichas, casilla_id) {
                let nfichas = fichas.length;

                fichas.forEach(function(ficha, i) {
                    if (ficha.currentcell !== casilla_id) {
                        if (nfichas > 1)
                            i++;

                        if (false)
                            ficha.movehtmlto(tablero_def, casilla_id, i, callendcallback);
                        else {
                            let p = tablero_def.offset(casilla_id, i, ficha.$);
                            ficha.movehtml(p, callendcallback);
                            ficha.currentcell = casilla_id;
                        }
                    }
                });
            })
            */
        }
    }

    /**
     * Dado un color, que fichas puede mover con una tirada
     * @param color el jugador que se esta comprobando
     * @param valortirada el numero de posiciones que debe poder avanzar la ficha
     * @param sacaficha indica si se debe intentar sacar ficha (de esta forma, el control dira si es por un cinco o doble cinco)
     * @param abrepuente indica si se debe abrir puente (de esta forma, el control dira si es por un doble o un 6)
     * @return lista de fichas que se pueden mover dada la tirada
     */
    fichasposibles(color, valortirada, sacaficha = false, abrepuente = false) {
        let fichasposibles = [];
        let self = this;

        if (sacaficha) {
            fichasposibles = this.fichasencasa(color).filter((ficha) => (self._cabeficha(ficha, ficha.principio)));
            if (fichasposibles.length > 0) {
                return fichasposibles;
            }
        }

        if (abrepuente) {
            let puentes = this.puentes(color, false);
            if (puentes.length > 0) {
                puentes = puentes.fichas().filter((f) => (self._movimientovalido(f, valortirada)));
                if (puentes.length > 0)
                    return puentes.filter((f) => (f.tipo === color));
            }
        }

        let fichasdeljugador = this.casillas.fichas(color);

        fichasposibles = fichasdeljugador.filter(function(f) { 
            // Vamos a ver el camino que pasaria la ficha
            return this._movimientovalido(f, valortirada);
        }.bind(this))

        return fichasposibles;
    }

    fichascasilla(casillaid) {
        let fichascasilla = this.casillas[casillaid];
        if (fichascasilla === undefined) return [];
        return fichascasilla;
    }

    _movimientovalido(ficha, valortirada) {
        let self = this;
        let breadcrumb = ficha.breadcrumb(ficha.cell2pos(ficha.currentcell), valortirada);
        if ((breadcrumb === false)||(breadcrumb === null)) return false;

        // Si no cabe, no tiene sentido que sigamos
        let casillafinal = breadcrumb[breadcrumb.length - 1];
        if (!this._cabeficha(ficha, casillafinal))
            return false;

        let conflictos = breadcrumb.filter(
            function(casillaid) { 
                let fichasencasilla = self.fichascasilla(ficha.pos2cell(casillaid));
                if (fichasencasilla.length === 2) {
                    if (fichasencasilla[0].tipo === fichasencasilla[1].tipo)
                    return true;
                }
                return false;
        });

        return (conflictos.length === 0);
    }    

    /**
     * Funcion que hace el movimiento de la ficha seleccionada por el jugador "player", cuando ha sacado unos dados en "values"
     * @param player el jugador que hace el movimiento
     * @param valortirada
     * @param ficha ficha seleccionada para mover
     * @return true si corre el turno o false si mantiene el turno
     */
    realizarmovimiento(player, valortirada, sacaficha, repitetirada, ficha) {

        // Esto es un poco redundante, pero vamos a hacer un "descartado rapido" de la ficha erronea
        let fichaseleccionada = this.fichasposibles(player.tipo, valortirada, sacaficha, repitetirada).filter((x) => (x.id === ficha.id));
        if (fichaseleccionada.length !== 1) {
            return false;
        }
        
        let fichamuerta = false;
        let fichacasa = false;

        if (ficha.cell2pos(ficha.currentcell) === null) {
            // Evaluamos si se va a matar una ficha
            fichamuerta = this.mataficha(ficha, ficha.principio);

            // Metemos la ficha muerta (porque si no, no podremos sacar la otra)
            if (fichamuerta !== false)
                this.meteficha(fichamuerta);

            // Sacamos la ficha
            this.sacaficha(ficha);
        } else {
            // Estamos moviendo una ficha, y el movimiento es valido
            let casilladestino = ficha.pos2cell(ficha.destpos(valortirada));
            // let breadcrumb = ficha.breadcrumb(ficha.cell2pos(ficha.currentcell), valortirada);
            // let casilladestino = ficha.pos2cell(breadcrumb[breadcrumb.length - 1]);

            // Evaluamos si se va a matar una ficha
            fichamuerta = this.mataficha(ficha, casilladestino);

            this._moverficha(ficha, ficha.currentcell, casilladestino);

            if (casilladestino === 0) {
                fichacasa = ficha;
            }
        }
        this._ultimaficha[ficha.tipo] = ficha;

        return {
            muerta: fichamuerta,
            casa: fichacasa
        }
    }
    
    /**
     * Funcion que saca una ficha (cualquiera) del jugador indicado, si cabe en la casilla correspondiente, claro.
     * @param player jugador que quiere sacar la ficha
     */
    sacaunaficha(player) {
        let fichasencasa = this.fichasencasa(player.tipo);
        if (fichasencasa.length === 0) return false;
        return this.sacaficha(fichasencasa[0]);
    }

    meteficha(ficha) {

        let casilla = null;
        // Primero buscamos la casilla en la que esta
        this.casillas.forEach(function(fichas, casilla_id) {
            if (casilla !== null) return;
            if (fichas.indexOf(ficha) !== -1) {
                // La hemos encontrado
                casilla = casilla_id;
            }
        })

        if (casilla !== null) {
            this._quitarficha(ficha, casilla)
        }

        let pos = this.home[ficha.tipo].indexOf(ficha);

        // Ya esta en casa
        if (pos !== -1) 
            return true;

        // Buscamos un hueco y, si no, al final
        pos = this.home[ficha.tipo].indexOf(null);
        if (pos === -1)
            pos = this.home[ficha.tipo].length;

        this.home[ficha.tipo][pos] = ficha;
        return true;
    }

    /**
     * Saca la ficha indicada a la casilla correspondiente (si cabe)
     */
    sacaficha(ficha) {
        let pos = this.home[ficha.tipo].indexOf(ficha);
        if (pos === -1) {
            return false;
        }

        if (!this._cabeficha(ficha, ficha.principio)) {
            return false;
        }

        // Quitamos la ficha del home
        this.home[ficha.tipo][pos] = null;

        // Ponemos la ficha en su inicio
        this._ponerficha(ficha, ficha.principio);
        return true;
    }

    /**
     * Devuelve la lista de fichas muertas si se mueve una ficha a una casilla (o false si no se mata ninguna)
     * 
     * @param ficha la ficha que se mueve
     * @param f_casilla_id la casilla de destino
     * @return lista de fichas muertas o "false" si no se mata ninguna
     */
    mataficha(ficha, f_casilla_id) {
        let fichascasilla = this.fichascasilla(f_casilla_id);
        if (fichascasilla.length === 0) return false;
        //if ((this.casillas[f_casilla_id] === undefined) || (this.casillas[f_casilla_id].length === 0)) return false;

        // El caso especial de sacar una ficha en la casa, se matara la ultima que hubiera llegado, si hay 2 (o la primera, si es de otro color)
        if (f_casilla_id === ficha.principio) {
            if (fichascasilla.length === 2) {
                if (fichascasilla[1].tipo !== ficha.tipo) {
                    return fichascasilla[1];
                }
                if (fichascasilla[0].tipo !== ficha.tipo) {
                    return fichascasilla[0];
                }
            }
            return false;
        }

        // Si ya hay 2 fichas, no se matara ninguna, porque no cabra
        if (fichascasilla.length === 2) return false;

        // En este punto hay solo 1 ficha en la casilla...

        // Si es del mismo tipo, no se mata: se hace puente
        if (fichascasilla[0].tipo === ficha.tipo) return false;

        // Si no es un seguro, se matara la ficha que haya
        if (this.tablero_def.seguro(f_casilla_id) !== true)
            return fichascasilla[0];

        return false;
    }

    /**
     * Pone una ficha en una casilla
     * @param ficha la ficha que se quiere poner
     * @param f_casilla_id el identificador de la casilla donde se quiere poner
     * @return la ficha (false si no se ha podido)
     */
    _ponerficha(ficha, f_casilla_id) {
        if (f_casilla_id === 0) {
            this._finalizadas[ficha.tipo].push(ficha);
            return ficha;
        }

        if (this.casillas[f_casilla_id] === undefined)
            this.casillas[f_casilla_id] = [];

        if (this.casillas[f_casilla_id].length >= 2) {
            // No hemos podido poner la ficha en la casilla
            return false;
        }

        this.casillas[f_casilla_id].push(ficha);
        return ficha;
    }

    /**
     * Quita la ficha de la casilla indicada
     * @param ficha la ficha que se quiere quitar de la casilla
     * @param f_casilla_id la casilla de la que se quiere quitar
     * @return la ficha o "false" si no se ha podido
     */
    _quitarficha(ficha, f_casilla_id) {
        if (this.casillas[f_casilla_id] === undefined) {
            return false;
        }
        let pos = this.casillas[f_casilla_id].indexOf(ficha);
        if (pos === -1) {
            return false;
        }

        this.casillas[f_casilla_id].splice(pos, 1);
        return ficha;
    }

    _cabeficha(ficha, f_casilla_id) {
        if (f_casilla_id === 0) 
            return (this._finalizadas[ficha.tipo].length < 4);

        if (this.casillas[f_casilla_id] === undefined) return true;
        if (this.casillas[f_casilla_id].length == 2) {
            // Si va a salir al principio y hay alguna ficha de otro color, si que cabe (matará a la última de otro color)
            if (f_casilla_id === ficha.principio) {
                let fichasotrocolor = this.casillas[f_casilla_id].reduce((total, f) => (total + (f.tipo !== ficha.tipo)?1:0), 0);
                if (fichasotrocolor > 0) 
                    return true;
            }
            return false;
        }
        return true;
    }

    _moverficha(ficha, o_casilla_id, d_casilla_id) {
        if (this._cabeficha(ficha, d_casilla_id) !== true) {
            return false;
        }
        if (this._quitarficha(ficha, o_casilla_id) === false) {
            return false;
        }
        if (this._ponerficha(ficha, d_casilla_id) === false) {
            return false;
        }
        return true;
    }
}

class CellArray extends Array {
    forEach(callback) {
        for (let idx in this) {
            if (!isNaN(parseInt(idx)))
                idx = parseInt(idx);
            let elem = this[idx];
            callback(elem, idx);
        }
    }
    reduce(callback, start = 0) {
        for (let idx in this) {
            if (!isNaN(parseInt(idx)))
                idx = parseInt(idx);
            let elem = this[idx];
            start = callback(start, elem, idx);
        }
        return start;
    }
    filter(callback) {
        let result = new CellArray();
        for (let idx in this) { 
            if (!isNaN(parseInt(idx)))
                idx = parseInt(idx);
            let elem = this[idx];
            if (callback(elem, idx) === true) 
                result[idx] = elem;
        }
        return result;
    }
    fichas(tipo = null) {
        if (tipo === null)
            return this.reduce((a,x) => ([ ...a, ...x]), []);
        else
            return this.reduce((a,x) => ([ ...a, ...x.filter(f => f.tipo === tipo)]), []);
    }
}

function unique(a) {
    return a.filter((value, index) => (a.indexOf(value) === index));
}
