class GameControl {
    // TODO: hacer que las fichas sean propiedad de un jugador, que no se use el type sino el "id"; el que sean rojas, azules, etc. es coyuntural

    constructor(tablero) {
        this._tablero = tablero;
        this._jugadores = [];
        this._pausa = true;
        this.endgamecallback = null;
        this.preparapartida(
            {
                rojo: "ordenador", 
                verde: "ordenador", 
                azul: "ordenador", 
                amarillo: "ordenador",
                tiempo: 30, 
                fichafuera: 1
            }, null
        )
    }

    _creajugador(tipo, color) {
        let $tablero = $('#tablero');
        let x = "r";
        let nombre = undefined;
        switch (color) {
            case F_ROJO: x = "r"; nombre="rojo"; break;
            case F_VERDE: x = "v"; nombre="verde";break;
            case F_AZUL: x = "z"; nombre="azul";break;
            case F_AMARILLO: x = "a"; nombre="amarillo";break;
        }
        let dado = creadado($tablero, this._tablero.tablero_def, `d${x}`, `${x}`, 34);
        switch(tipo) {
            case 'humano': return new Player_Humano(color, dado, nombre);
            case 'ordenador': return new Player(color, dado, nombre);
            default: return null;
        }
    }

    preparapartida(configuracion, endgamecallback = function() {}) {
        // Eliminamos los recursos de los jugadores
        this._jugadores.forEach((x) => (x.cleanup()));

        this.endgamecallback = endgamecallback;

        // Creamos los nuevos jugadores
        configuracion.rojo = this._creajugador(configuracion.rojo, F_ROJO);
        configuracion.verde = this._creajugador(configuracion.verde, F_VERDE);
        configuracion.amarillo = this._creajugador(configuracion.amarillo, F_AMARILLO);
        configuracion.azul = this._creajugador(configuracion.azul, F_AZUL);
        this._jugadores = [ configuracion.rojo, configuracion.verde, configuracion.amarillo, configuracion.azul ].filter((x) => (x!==null));

        // Reiniciamos el tablero
        this._tablero.reinicia(this._jugadores);

        // Establecemos el tiempo
        configuracion.tiempo = parseInt(configuracion.tiempo);
        if (!isNaN(configuracion.tiempo)) {
            this._tiradatimeoutcallback = function(elapsed) {
                tiempo(elapsed, configuracion.tiempo);
                return (elapsed >= configuracion.tiempo);
            }
        } else {
            this._tiradatimeoutcallback = (elapsed)=>(false);
        }

        let self = this;
        while (configuracion.fichafuera > 0) {
            this._jugadores.forEach((x) => (self._tablero.sacaunaficha(x)));
            configuracion.fichafuera--;
        }

        this.empiezapartida();
    }

    empiezapartida(player = null) {
        let self = this;
        this._jugadores.forEach(function(j) {
            j.asignarfichas(self._tablero.fichas(j.tipo))
        });

        if (player === null) {
            let playerid = getRandomNumber(0, this._jugadores.length - 1);
            player = this._jugadores[playerid]
        }
        this.establecerturno(player);

        showcomment(`comienza el juego`);

        this.jugarturno();
    }

    establecerturno(player) {
        let estado = {
            t: player.tipo,
            b: this._tablero.save()
        }

        // Guardamos la cadena de estado
        // console.log(btoa(JSON.stringify(estado)));

        // Deshabilitaremos todas las fichas y todos los dados
        this._jugadores.forEach((x) => (x.dado.disable().$.removeClass('turno')));
        this._tablero.fichas().forEach((x) => (x.disable().$.removeClass('turno')));

        // El jugador nuevo *no* ha repetido
        if (this._jugador !== player) {
            this._repeticiones = 0;
        }

        this._jugador = player;
        this._jugador.turno();
    }

    /**
     * Funcion que comprueba si ha pasado demasiado tiempo desde que se inicio el turno y se tiene que forzar
     */
    _tiempoturno(callback, inicio) {
        let self = this;

        if (inicio === undefined) {
            inicio = new Date();
        }

        let t = new Date();
        let elapsed = (t - inicio) / 1000;
        let rollnow = false;

        // Solo vamos a permitir un evento de timeout al tiempo
        this._cleartimeout();

        if (typeof(this._tiradatimeoutcallback) === 'function') {
            rollnow = this._tiradatimeoutcallback(elapsed);
        }

        if (rollnow === true) {
            callback();
        } else {
            this._tiradatimeout = setTimeout(function() {
                self._tiempoturno(callback, inicio);
            }, 1000);
        }
    }

    pausa() {
        this._pausa = true;
    }

    continuar() {
        this.establecerturno(this._jugador);
        this.jugarturno();
    }

    jugarturno() {
        let self = this;
        this._tablero.actualizar();
        this._fichamuerta = null;
        this._fichacasa = null;

        if (this._pausa) {
            // Pausamos la partida

            this._pausa = false;
            return;
        }

        // Marcamos el inicio del turno y quitamos el timeout para forzar la tirada
        this._cleartimeout();

        // Generamos un resultado del dado
        this._resultado_tirada = parseInt($('#valordado').val());
        if (isNaN(this._resultado_tirada) || (this._resultado_tirada > 6) || (this._resultado_tirada < 0))
            this._resultado_tirada = getRandomNumber(1, 6);

        // Si el jugador es interactivo, pondremos un timeout para que el jugador juegue, para asegurarnos de que se avanza
        if (this._jugador.interactivo)
            this._tiempoturno(function() {
                self._jugador.forzartirada();
            });

        // Programamos al jugador para que tire... y ya llamara al resultado de la tirada
        this._jugador.tirar(
            self._resultado_tirada, // Le indicamos el valor que va a sacar en el dado
            function() {
                self._tiradarealizada();
            }
        );
    }

    /**
     * Funcion que elimina un timeout, si es que existia
     */
    _cleartimeout() {
        if (this._tiradatimeout !== null) {
            clearTimeout(this._tiradatimeout);
        }
        this._tiradatimeout = null;
    }

    /**
     * Funcion que se llama cuando se ha realizado una tirada de dados; esta desacoplado de lo que seria el "jugar el turno" para permitir usuarios interactivos
     */
    _tiradarealizada() {
        // Quitamos el timeout, si es que habia
        this._cleartimeout();

        // Deshabilitamos los dados
        this._jugadores.forEach((x) => (x.dado.disable()));

        // Filtramos la tirada, para ver si tendra que tirar otra vez o si pierde el turno, o si se modifica (6+1)
        if (this._filtrartirada()) {
            let self = this;
            this._tablero.moverfichas(function() {
                self._findeturno();
            });
        } else {
            this._realizarmovimiento();
        }
    }

    /**
     * 
     * @param avances cantidad de casillas que debe avanzar
     */
    _realizarmovimiento() {

        // Al comenzar un movimiento no hay ninguna ficha; y esto se tiene que volver a poner aqui porque despues de un movimiento puede
        //   haber otro: si se mata una ficha o si se mete una ficha en casa
        if (this._fichamuerta !== null) {
            this._fichamuerta = null;
            this._resultado_tirada = 20;
        }
        if (this._fichacasa !== null) {
            this._fichacasa = null;
            this._resultado_tirada = 10;
        }
        let avances = this._resultado_tirada;

        // Miramos a ver que fichas se pueden mover con la tirada actual
        let fichasposibles = this._tablero.fichasposibles(this._jugador.tipo, avances, avances === 5, [6, 7].indexOf(avances) !== -1);
        let self = this;

        if (fichasposibles.length === 0) {
            switch (this._resultado_tirada) {
                case 20:
                    return showcomment(`El jugador <strong>${this._jugador.name}</strong> no se puede contar las 20`, this._jugador, 70, function() {
                        self._findeturno();}); break;
                case 10:
                    return showcomment(`El jugador <strong>${this._jugador.name}</strong> no se puede contar las 10`, this._jugador, 70, function() {
                        self._findeturno();}); break;
                default:
                    showcomment(`El jugador <strong>${this._jugador.name}</strong> no puede mover`, this._jugador); break;    
            }
            
            this._findeturno();
            return;
        }

        function hacerqueeljugadorseleccioneficha(self) {
            // Si el jugador es interactivo, pondremos un timeout para que el jugador juegue, para asegurarnos de que se avanza
            if (self._jugador.interactivo)
                self._tiempoturno(function() {
                    self._jugador.forzarseleccionficha(fichasposibles, self._tablero, self._resultado_tirada);
                });

            // Hacemos que el jugador seleccione una ficha
            self._jugador.seleccionarfichaamover(fichasposibles, self._tablero, self._resultado_tirada,
                function(ficha) {
                    self._fichaseleccionada(ficha);
                }
            );        
        }

        if ((avances === 5) && (fichasposibles[0].currentpos === null)) {
            return showcomment(`El jugador <strong>${this._jugador.name}</strong> saca una ficha`, this._jugador, 25, function() {
                hacerqueeljugadorseleccioneficha(self);
            })
        }

        if ((avances >=6 ) && (this._tablero.puentes(this._jugador.tipo, false).length > 0)) {
            return showcomment(`El jugador <strong>${this._jugador.name}</strong> tiene que abrir un puente`, this._jugador, 25, function() {
                hacerqueeljugadorseleccioneficha(self);
            })
        }

        hacerqueeljugadorseleccioneficha(self);
    }

    _fichaseleccionada(fichaseleccionada) {
        // Eliminamos el timeout
        this._cleartimeout();

        let avances = this._resultado_tirada;

        // Movemos y todo eso...
        let movimiento = false;
        if ((movimiento = this._tablero.realizarmovimiento(this._jugador, avances, avances === 5, [6,7].indexOf(avances)!==-1, fichaseleccionada)) === false) {
            console.log(`el jugador ${this._jugador.name} ha seleccionado una ficha no valida, asi que pierde el turno`)
        } else {
            if (movimiento.muerta !== false) {
                this._fichamuerta = movimiento.muerta;
                this._tablero.meteficha(this._fichamuerta);
            }
            if (movimiento.casa !== false) {
                this._fichacasa = movimiento.casa;
            }
            let casilladestino = fichaseleccionada.destpos(avances);
            if (this._tablero.fichascasilla(casilladestino).length > 1)
                showcomment(`El jugador <strong>${this._jugador.name}</strong> ha hecho un puente`, this._jugador);
            else {
                if (this._tablero.tablero_def.seguro(casilladestino) === true)
                    showcomment(`El jugador <strong>${this._jugador.name}</strong> ha llegado a un seguro`, this._jugador);
            }
        }

        this._findeturno();
    }

    /**
     * Esta funcion se utiliza para finalizar el turno actual y evaluar si se ha matado una ficha o no, o si se ha metido una
     *   ficha en casa, etc. También se encarga de evaluar si se debe continuar (ejecutando un siguiente turno) o si se ha de
     *   finalizar el juego. 
     * 
     * + En realidad se podría juntar con _siguienteturno, pero se ha separado por cuestiones didácticas.
     * 
     * - se llega aqui tras perder un turno o tras mover una ficha
     */
    _findeturno() {
        let self = this;
        this._tablero.moverfichas(function() {

            // Si el jugador actual no puede hacer nada, terminamos la partida
            if (self._tablero.fichasenjuego(self._jugador.tipo).length === 0) {
                self._fichamuerta = null;
                self._fichacasa = null;
                return self._siguienteturno();
            }

            if ((self._fichamuerta !== null) || (self._fichacasa !== null)) {
                if (self._fichamuerta !== null)
                    showcomment(`El jugador <strong>${self._jugador.name}</strong> ha matado una ficha`, self._jugador, 75, function() {
                        self._realizarmovimiento();
                    });
                else
                    showcomment(`El jugador <strong>${self._jugador.name}</strong> ha metido una ficha en casa`, self._jugador, 75, function() {
                        self._realizarmovimiento();
                    });
            }
            else
                self._siguienteturno();                
        });
    }

    /**
     * Esta funcion se desacopla de "_findeturno" por cuestiones puramente didacticas, pero en realidad no aporta nada el ternerla
     *   suelta. Simplemente se encarga de determinar si se debe pasar al siguiente jugador o no
     */
    _siguienteturno() {
        let siguiente = null;
        if (this._repeticiones > 0) {
            // Tiene que tirar otra vez
            siguiente = this._jugador;
            showcomment(`El jugador <strong>${this._jugador.name}</strong> tira otra vez`, this._jugador)
        } else {
            siguiente = this._jugadores[(this._jugadores.indexOf(this._jugador) + 1) % this._jugadores.length];
        }

        if (this._tablero._finalizadas.filter((x) => (x.length === 4)).length > 0) {
            // Alguien ha terminado
            if (typeof(this.endgamecallback) === "function") {
                this.endgamecallback(this._jugador, this);
            }
        } else {
            this.establecerturno(siguiente);
            this.jugarturno();
        }
    }

    /**
     * Funcion que observa el resultado de la tirada y lo modifica (si procede) y controla la perdida del turno y/o si se debe repetir tirada
     * @return "true" si pierde turno
     */
    _filtrartirada() {
        if (this._resultado_tirada === 6) {
            let fichasencasa = this._tablero.fichasencasa(this._jugador.tipo);
            if (fichasencasa.length === 0)
                this._resultado_tirada = 7;

            this._repeticiones++;

            if (this._repeticiones >= 3) {
                showcomment(`El jugador <strong>${this._jugador.name}</strong> ha repetido 3 veces, asi que pierde el turno`, this._jugador)

                let ultimafichamovida = this._tablero.ultimaficha(this._jugador.tipo);
                if (ultimafichamovida !== null) {
                    // Si una ficha esta en casa o ya en el camino a casa, no se mata por triple doble
                    if (ultimafichamovida.cell2pos(ultimafichamovida.currentcell) > 0) {
                        showcomment(`además, <strong>${this._jugador.name}</strong> pierde la última ficha movida`, this._jugador)
                        this._tablero.meteficha(ultimafichamovida);
                    }
                }
                this._repeticiones = 0;
                return true;
            }
        } else {
            this._repeticiones = 0;
        }
        return false;
    }

    get jugadores() {
        return this._jugadores;
    }

    load(s) {
        let estado = JSON.parse(atob(s));
        this._tablero.load(estado.b);
        let jugadores = this._jugadores.filter((x) => (x.tipo === estado.t ));
        if (jugadores.length === 0) { jugadores = this._jugadores; }
        this.establecerturno(jugadores[0]);
    }
}