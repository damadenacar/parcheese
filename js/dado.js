/**
 * Crea un objeto de tipo dado para que pueda ser utilizado por el jugador, y crea el objeto jquery asociado
 * 
 * @param $container objeto jquery que va a contener al dado
 * @param tablero objeto de definicion de tablero del que tenemos que obtener la posicion de la casilla
 * @param casilla casilla del tablero donde se va a colocar el dado
 * @param htmlclass clases adicionales que debe contener el div del dado
 * @param size tamaño del dado
 * @return un objeto de tipo Dado que hace referencia aun objeto jquery que ha sido añadido al contenedor
 */
function creadado($container, tablero, casilla, htmlclass = "", size = 30) {
    let dado = $(`<div class="text-center ${htmlclass} dice-container"></div>`);
    let dado_d = new Dado(dado, null, null, size);
    $container.append(dado);
    let d = tablero.offset(casilla, 0, $(dado_d.$.find('.face')[0]));
    dado.offset(d);
    dado_d.disable();
    return dado_d;
}

class Dado {
    /**
     * Construye el objeto. Se utilizan arrays para que sea más fácil crear dados múltiples (por ejemplo, 2 dados)
     * @param container el contenedor jquery donde se van a ubicar los Dados
     * @param beginrollcallback la funcion que se debe invocar al ir a rodar los dados function(valores) {}
     * @param rollcallback la funcion que se debe invocar al terminar de rodar los dados function(valores) {}
     * @param size el tamaño en pixels deseado para los dados (es importante, para los desplazamientos de las caras)
     */
    constructor(container, beginrollcallback = function (){}, endrollcallback = function (){}, dicesize = 60) {
        this._container = container;
        this.$ = null;
        this._beginrollcallback = beginrollcallback;
        this._endrollcallback = endrollcallback;
        this._build(dicesize);
    }

    /**
     * Habilita o deshabilita (en funcion del parametro "enable") los dados. Lo hace añadiendo o quitando la clase "disabled" a
     *   los objetos html
     */
    enable(enable = true) {
        if (enable === true)
            this.$.removeClass('disabled'); 
        else 
            this.$.addClass('disabled');
        return this;
    }

    /**
     * Deshabilita los dados, utilizando el metodo "enable(false)"
     */
    disable() {
        return this.enable(false);
    }

    /**
     * Construye los objetos que forman el/los dados
     */
    _build(size) {
        this.$ = this._createdice(size);
        this._container.append(this.$);
    }

    /**
     * Funcion que crea un objeto de tipo dado, con el tamaño indicado: crea el div y llama a la funcion "dice" para que se
     *  ejecute el componente jQuery correspondiente. Tambien añade los callbacks para que se invoque al giro de dados cuando
     *  se haga click y para que se invoque a la terminacion de girar cuando acabe la animacion
     */
    _createdice(size) {
        let halfsize = size / 2.0;
        let dotsize = size / 5.0;
        let id = makeid(8);
        let dice = $(`<div id="${id}"></div>`).dice({size: size + "px", halfsize: halfsize + "px", dotsize: dotsize + "px"});

        // Add a callback to roll the dice
        dice.on('click', function() {
            this.roll()
        }.bind(this));

        // Add a callback to end the rolling
        dice.on('transitionend', function() {
            this.endroll(dice)
        }.bind(this));
        return dice;
    }

    /**
     * Establece los callbacks para el dado
     * @param callback1 
     * @param callback2 
     * 
     * setcallbacks(endrollcallback)
     * setcallbacks(beginrollcallback, endrollcallback)
     */
    setcallbacks(callback1, callback2) {
        if ((callback1 !== undefined) && (callback2 === undefined))
            this._endrollcallback = endrollcallback;

        if ((callback1 !== undefined) && (callback2 !== undefined)) {
            this._endrollcallback = callback2;
            this._beginrollcallback = callback1;
        }
    }

    /**
     * Funcion que gira los dados; recibe como parametro un array que contiene los valores para los dados (los que falten o sean "null" no se establecen)
     * @param values array de valores para los dados; si es un unico valor, se considera que es para todos
     * 
     * roll()
     * roll(values)
     * roll(values, endrollcallback)
     * roll(values, beginrollcallback, endrollcallback)
     */
    roll(values, callback1, callback2) {
        if ((callback1 !== undefined) && (callback2 === undefined))
            this._endrollcallback = callback1;

        if ((callback1 !== undefined) && (callback2 !== undefined)) {
            this._endrollcallback = callback2;
            this._beginrollcallback = callback1;
        }

        // Ejecutamos el callback, si procede
        if (typeof(this._beginrollcallback) === 'function') {
            values = this._beginrollcallback(values, this);
        }

        // Si hay un dado girando, no giramos de nuevo
        if (this.$.hasClass('rolling'))
            return false;

        // Ponemos los dados a girar
        this.$.addClass('rolling')
            .dice('roll', values);

        return true;
    }

    /**
     * Funcion que se invoca cuando terminan de girar un dado; recibe como parametro el dado que termina de girar.
     *   - Cuando terminan de girar todos los dados llama al callback establecido en el constructor, indicando los valores de los dados
     */
    endroll(dice) {
        dice
        .removeClass('rolling');

        if (typeof(this._endrollcallback) === 'function')
            this._endrollcallback(this.$.dice('value'), this);
    }

    cleanup() {
        this.$.remove();
    }

    value() {
        return this.$dice('value');
    }
}

class Dados extends Dado {
    _build(size) {
        // TODO: hay que añadir bien los dados
        this.$ = [ this._createdice(size), this._createdice(size) ];
        let w = "80px";
        let res = $(`<div class="row" style="width:${w}"></div>`);
        this.$.forEach(function(d) {
            res.append($('<div class="col"></div>').append(d));
        }.bind(this));
        this._container.append(res);
    }
}
